import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import {AppComponent} from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {AppMaterialThemeModule} from './app-material-theme/app-material-theme.module';
import {AuthenticationService} from './services/authentication.service';
import {CanActivateRouteGuard} from './can-activate-route.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from './jwt-interceptor.service';
import { NotesService} from './services/notes.service';
import { RouterService} from './services/router.service';
import {BrowserModule} from '@angular/platform-browser';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatCardModule,MatToolbarModule,MatSelectModule} from '@angular/material';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {NoteViewComponent} from './note-view/note-view.component';
import {NoteComponent} from './note/note.component';
import {ListViewComponent} from './list-view/list-view.component';
import {EditNoteOpenerComponent} from './edit-note-opener/edit-note-opener.component';
import {EditNoteViewComponent}from './edit-note-view/edit-note-view.component';
import {NoteTakerComponent} from './note-taker/note-taker.component';

const appRoutes : Routes = [
{
  path : 'dashboard',
  component :DashboardComponent,
  canActivate : [CanActivateRouteGuard],
  children : [
      { path: 'view/noteview', component: NoteViewComponent },
      { path: 'view/listview', component: ListViewComponent },
      {path : '', redirectTo : 'view/noteview', pathMatch: 'full' }
  ]
},{
    path : 'login',
  component :LoginComponent

},
{
    path: "note/:noteId/edit",
    component: EditNoteOpenerComponent ,
    outlet: "noteEditOutlet"
  },
{
   path: '', redirectTo: 'login', pathMatch: 'full' 
}
]
@NgModule({
  declarations: [ AppComponent,HeaderComponent,DashboardComponent,LoginComponent,NoteViewComponent,ListViewComponent,EditNoteOpenerComponent,NoteTakerComponent,NoteComponent,EditNoteViewComponent],
  imports: [AppMaterialThemeModule,RouterModule.forRoot(appRoutes, {enableTracing : true}),HttpModule,HttpClientModule,BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    FormsModule,
    HttpModule,
    MatSelectModule,
    ReactiveFormsModule,
    CommonModule],
  providers: [AuthenticationService,CanActivateRouteGuard,NotesService,RouterService,
  {
  	provide : HTTP_INTERCEPTORS,
  	useClass : JwtInterceptor,
  	multi : true
  } ],
  bootstrap: [ AppComponent]
})

export class AppModule { }
