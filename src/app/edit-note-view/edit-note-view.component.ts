import { Component,Input } from '@angular/core';
import { Note } from '../note';
import {HttpErrorResponse} from '@angular/common/http'
import { NotesService } from '../services/notes.service';
import { RouterService } from '../services/router.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Component({
  selector: 'app-edit-note-view',
  templateUrl: './edit-note-view.component.html',
  styleUrls: ['./edit-note-view.component.css']
})
export class EditNoteViewComponent {
  @Input('note') note: Note;
  
  notes : Note[];
  states: Array<string> = ['not-started', 'started', 'completed'];
  errMessage: string;
 constructor(private noteService: NotesService,private routerService: RouterService) { 

 }
  onSave() {
    this.notes=this.noteService.notes;
   
 this.errMessage = '';

     this.noteService.editNote(this.note).subscribe( note=> {
      
      this.routerService.routeBack();
   },
    //error => this.errorMessage = <any>error);
    error=>this.handleErrorResponse(error));
     this.noteService.getNotes();
 }
  handleErrorResponse(error: HttpErrorResponse): void {
   // error to display when it is failure

   if (error.status === 404) {
     this.errMessage = `Http failure response for ${error.url}: 404 Not Found`;
   } else {
     this.errMessage = 'An error occurred:' + error.error.message;
   }
 }
}
