import { Component,Input,OnInit } from '@angular/core';
import {Note} from '../note';
import {HttpErrorResponse} from '@angular/common/http'
import { NotesService } from '../services/notes.service';
@Component({
  selector: 'app-note-view',
  templateUrl: './note-view.component.html',
  styleUrls: ['./note-view.component.css']
})
export class NoteViewComponent implements OnInit{

 notes : Note[];
 errorMessage: String;
  constructor(private noteService: NotesService) { }
 errMessage: String;
  ngOnInit(): void {
   
    
    
   this.noteService.notesSubject.subscribe(

    notes=> this.notes=notes,

    error => this.handleErrorResponse(error));
    
  }



handleErrorResponse(error: HttpErrorResponse): void {
   // error to display when it is failure

   if (error.status === 404) {
     this.errorMessage = `Http failure response for ${error.url}: 404 Not Found`;
   } else {
     this.errorMessage = 'An error occurred:' + error.error.message;
   }
 }
  
}
