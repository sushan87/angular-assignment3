import { Component,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Note} from '../note';
import { ActivatedRoute, Params } from '@angular/router';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { NotesService } from '../services/notes.service';
@Component({
  selector: 'app-edit-note-opener',
  templateUrl: './edit-note-opener.component.html',
  styleUrls: ['./edit-note-opener.component.css']
})
export class EditNoteOpenerComponent {
 
 noteId : String
 note : Note;
constructor(private route: ActivatedRoute,private router: Router,private notesService : NotesService) {
router.events.subscribe( (event: Event) => {

          
            if (event instanceof NavigationStart) {
               console.log('navigation starts');
            }

            if (event instanceof NavigationEnd) {
                  console.log('navigation ends');
                  this.note = this.notesService.getNoteById( this.route.snapshot.params.noteId);
            }

            if (event instanceof NavigationError) {
               
                console.log(event.error);
            }

 });


    
  }
}


    

