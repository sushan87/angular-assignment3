import { Component,OnInit } from '@angular/core';
import {RouterService} from '../services/router.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{
  isNoteView = true;
  constructor(private router : RouterService){

  }
  ngOnInit() {
    this.switchToNoteView();
  }
  switchToListView(){
  	
  	this.isNoteView = false;
  	this.router.routeToListView();
  }

  switchToNoteView(){
	
    this.isNoteView = true;
    this.router.routeToNoteView();
  }
  
}
