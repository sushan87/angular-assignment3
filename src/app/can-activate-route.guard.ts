import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './services/authentication.service';
import { RouterService } from './services/router.service';
@Injectable()
export class CanActivateRouteGuard implements CanActivate {

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private routerService: RouterService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const bearerToken = localStorage.getItem('bearerToken');
    
    const authenticationStatus = this.authenticationService.isUserAuthenticated(bearerToken).then(
      (data) => {
        return data;
      }, (err) => {
      });

    if (authenticationStatus) {
       
      return true;
    } else {
      this.routerService.routeToLogin();
      return false;
    }
  }
}
