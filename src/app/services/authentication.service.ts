import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable()
export class AuthenticationService {

   private baseUrl = 'http://localhost:3000/auth/v1/';


  constructor(private http: HttpClient) {

  }

  authenticateUser(data) {
    return this.http.post(this.baseUrl, data);
  }

  setBearerToken(token) {
    localStorage.setItem('bearerToken', token);
  }

  getBearerToken() {
    return localStorage.getItem('bearerToken');
  }

  isUserAuthenticated(token): Promise<boolean> {
    return new Promise((resolve, rej) => {
      this.http.post(this.baseUrl + 'isAuthenticated', {
        headers: {
          'Authorization': this.getBearerToken()
        }
      }).subscribe(res => {
        resolve(res['isAuthenticated']);
        // return res["isAuthenticated"];
      }, err => {
        rej(err);
        // return err;
      });
    });
  }
}
