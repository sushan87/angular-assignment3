import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Injectable,Output,EventEmitter } from '@angular/core';
import { Note } from '../note';
import 'rxjs/add/observable/throw';
import {HttpClient} from '@angular/common/http';
import {Response,RequestOptions} from '@angular/http';
import {HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {HttpErrorResponse} from '@angular/common/http'
@Injectable()
export class NotesService {
  url = "http://localhost:3000/api/v1/notes";
  editNoteUrl = "http://localhost:3000/api/v1/notes/";
  notes: Array<Note>;
  notesSubject: BehaviorSubject<Array<Note>>;
  
  constructor(private http:HttpClient) { 
     
    this.notesSubject = this.getNotes();
    

  }

  private extractData(res: Response) {
    
    let body = res;
    return body;
  }

  fetchNotesFromServer() : Observable<Array<Note>> {
    return this.http.get(this.url)
    .map(this.extractData)
    .catch(this.handleErrorObservable);
    ;
  }

  getNotes(): BehaviorSubject<Array<Note>> {
    this.notesSubject = new BehaviorSubject<Array<Note>>(new Array<Note>());
     this.fetchNotesFromServer().subscribe(
      notes => {this.notes = notes; this.notesSubject.next(this.notes)},
      error =>  this.handleErrorObservable(error));
     return this.notesSubject;
  }

  addNote(note: Note): Observable<Note> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.url, note)
    .map(this.extractData)
    .catch(this.handleErrorObservable);
  }

  editNote(note: Note): Observable<Note> {
 let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(this.editNoteUrl+note.id, note)
    .map(this.extractData)
    .catch(this.handleErrorObservable);
  }

  getNoteById(noteId:String): Note {
    let returnNote : Note = null;
    this.notesSubject.subscribe((notes)=>{
      this.notes = notes;
      this.notes.forEach(note=>{
        if(note.id.toString()===noteId)
        {    
            returnNote = note;
            
        }
    })
    });
    return returnNote;
 
  }
   private handleErrorObservable (error: HttpErrorResponse | any) {
    console.error(error.message || error);
    
    return Observable.throw(new HttpErrorResponse(error));
  }
}
