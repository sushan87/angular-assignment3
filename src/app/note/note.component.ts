import {Component, Inject,Input} from '@angular/core';
import {Note} from '../note';
import {EditNoteOpenerComponent} from '../edit-note-opener/edit-note-opener.component';
import {RouterService} from '../services/router.service';
/**
 * @title Dialog Overview
 */
@Component({
  selector: 'app-note',
  templateUrl: 'note.component.html',
  styleUrls: ['note.component.css'],
})
export class NoteComponent {

  @Input('note') note: Note;
   


  constructor(private routerService : RouterService) {}

  click() : void {
  	
  	this.routerService.routeToEditNoteView(this.note.id);
  }

}

