import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../services/authentication.service';
import { RouterService } from '../services/router.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = new FormControl();
  password = new FormControl();

  submitMessage: string;

  constructor(private authenticationService: AuthenticationService,
    private router: Router,
    private routerService: RouterService) { }

  ngOnInit() {
    this.username.validator = Validators.required;
    this.password.validator = Validators.required;
  }

  loginSubmit() {
    this.submitMessage = null;
    if (!this.username.value || this.username.value.trim() === ''
      || !this.password.value || this.password.value.trim() === '') {
      return;
    } else {
      const params = { 'username': this.username.value, 'password': this.password.value };
      this.authenticationService.authenticateUser(params)
        .subscribe(res => {
          this.authenticationService.setBearerToken(res['token']);
          this.submitMessage = null;
          this.routerService.routeToDashboard();
        }, err => {
          if (err.status === 404) {
            this.submitMessage = err.message;
          } else {
            this.submitMessage = err.error.message;
          }
        });
    }
  }
}
