import {Injectable} from '@angular/core';
import {HttpInterceptor,HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS} from '@angular/common/http';
import  {Observable} from 'rxjs';
import {Router} from '@angular/router';
@Injectable()
export class JwtInterceptor implements HttpInterceptor{
constructor(private router:Router ){}
	intercept(request:HttpRequest<any>, next : HttpHandler):Observable<HttpEvent<any>>{

		let userAuthToken = localStorage.getItem('bearerToken');

		if(userAuthToken){

			request = request.clone({
				setHeaders:{
					Authorization : 'Bearer ' + userAuthToken
				}
			});
			
		}else{
			console.log("jwt token NOT found");
			this.router.navigate(['/login']);
		}

		return next.handle(request);
	}
}