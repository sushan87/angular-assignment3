import { Component, OnInit,Input} from '@angular/core';
import { Note } from '../note';
import { NotesService } from '../services/notes.service';
import {HttpErrorResponse} from '@angular/common/http'
import {map} from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']

})
export class ListViewComponent implements OnInit{
  notes : Note[];
  notStartedNotes: Array<Note>;
  startedNotes: Array<Note>;
  completedNotes: Array<Note>;
  errorMessage: String;

  constructor(private noteService: NotesService) { 
    this.noteService.notesSubject.subscribe(

    notes=> this.notes=notes,

    error => this.handleErrorResponse(error));
  }
  
  ngOnInit(){
    this.notStartedNotes = new Array<Note>();
    this.startedNotes = new Array<Note>();
    this.completedNotes = new Array<Note>();
    this.populateNotes();
    this.loadNotStartedNotes();
    this.loadStartedNotes();
    this.loadCompletedNotes();
  }

  populateNotes(){
    
  }
  loadNotStartedNotes(){

    
    this.noteService.notesSubject.subscribe(

    notes=> {this.notStartedNotes=notes.filter(note=>note.state==='not-started')},

    error => this.handleErrorResponse(error));
  		
   
      
  	
}
 loadStartedNotes(){
   
    this.noteService.notesSubject.subscribe(

    notes=> {this.startedNotes=notes.filter(note=>note.state==='started')},

    error => this.handleErrorResponse(error));
   
      
    
}
loadCompletedNotes(){
   
    this.noteService.notesSubject.subscribe(

    notes=> {this.completedNotes=notes.filter(note=>note.state==='completed')},

    error => this.handleErrorResponse(error));
}


handleErrorResponse(error: HttpErrorResponse): void {
   // error to display when it is failure

   if (error.status === 404) {
     this.errorMessage = `Http failure response for ${error.url}: 404 Not Found`;
   } else {
     this.errorMessage = 'An error occurred:' + error.error.message;
   }
 }
  
}
